# First - Normal Vars

Normal variable in `host_vars` directory

```sh
ansible-playbook site.yml
cat /tmp/RHUG.txt
```

# Second - New Vault

No variables, we are going to create a vault

```sh
ansible-vault create host_vars/localhost.yml
```

Password is: `12345`. It's super secure

Show that it's actually encrypted, and how to view it

```sh
cat host_vars/localhost.yml
ansible-vault view host_vars/localhost.yml
```

We can run a playbook off of this

```sh
ansible-playbook --ask-vault-pass site.yml
```

Files can make it easier to automate

```sh
echo '12345' > password.txt
ansible-playbook --vault-password-file site.yml
```

The above is deprecated. In fact, we should be doing this:

```sh
ansible-playbook --vault-id password.txt site.yml
```

You can try multiple password now with --vault-id

```sh
echo 'wrong_pass' > wrong.txt
ansible-playbook --vault-id @wrong.txt --vault-id @password.txt site.yml
```

Finally, vault-ids can make this even easier, labels!

```sh
ansible-vault --vault-id password.txt rekey --new-vault-id=RHUG@password.txt host_vars/localhost.yml
ansible-playbook --vault-id RHUG@wrong.txt --vault-id RHUG@password.txt site.yml
```

# Third - Embedded Strings

We may want to just encrypt one string. We have a password file and no variables. Variables are in `site.yml`

```sh
cat site.yml
ansible-vault encrypt_string --vault-id password.txt 'The contents are safe' --name 'some_variable'
```

# Fourth - Best Practices

Keep vaults in separate files, but for readability, have unencrypted.

***Show two files***
